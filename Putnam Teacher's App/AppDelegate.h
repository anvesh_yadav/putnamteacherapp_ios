//
//  AppDelegate.h
//  Putnam Teacher's App
//
//  Created by inficare on 5/13/16.
//  Copyright © 2016 inficare. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

