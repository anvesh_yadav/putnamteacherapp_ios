//
//  main.m
//  Putnam Teacher's App
//
//  Created by inficare on 5/13/16.
//  Copyright © 2016 inficare. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
